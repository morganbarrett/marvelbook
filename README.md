# Marvelbook

A character browser for the Marvel universe.

## Set Up
Open src/components/MarvelAPI.js and substitute your public and private keys for Marvels API service.

```js
import React, { Component } from "react";
import md5 from "../md5";
import "isomorphic-fetch";

const HASH_KEY = "ENTER YOUR PRIVATE KEY HERE",
    PUBLIC_KEY = "ENTER YOUR PUBLIC KEY HERE",
    MARVEL_URL = "https://gateway.marvel.com/v1/public/";
```

## Running
Launch a terminal window in the Marvelbook root directoy and run:
```bash
$ npm start
```

Then open http://localhost:3000/ to see the app.

When you’re ready to deploy to production, create a minified bundle with `npm run build`.

## Running Tests
Launch a terminal window in the Marvelbook root directoy and run:
```bash
$ npm test
```

---
#### Author
- [Morgan Barrett](http://www.morganbarrett.co.uk)
