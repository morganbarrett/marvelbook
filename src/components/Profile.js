import React, { Component } from "react";
import MarvelAPI from "./MarvelAPI";
import "../css/Profile.css";

class Profile extends MarvelAPI {
	constructor(props){
		super(props);
		this.state.type = "characters";
		this.state.id = this.props.match.params.id;
		this.call();
		this.renderSection = this.renderSection.bind(this);
	}

	componentWillReceiveProps(nextProps){
		if(this.state.id != nextProps.match.params.id){
			this.state.id = nextProps.match.params.id;
			this.call();
		}
	}

	renderSection(i){
		var character = this.state.data;

		if(character[i] && character[i].items && character[i].items.length > 0){
			var label = i.charAt(0).toUpperCase() + i.slice(1),
				arr = character[i].items.map(function(item){
					return <li>{item.name}</li>;
				});

			return (
				<section>
					<h3>{label}</h3>
					<ul>{arr}</ul>
				</section>
			);
		}
	}

	render(){
		var character = this.state.data,
			hasBio = character.description && character.description.replace(" ", "") != "",
			info = ["comics", "stories", "events", "series"],
			infoBody = info.map(this.renderSection);

		return (
			<div className="Main Profile">
				{character.thumbnail ? <img src={character.thumbnail.path + "/landscape_incredible." + character.thumbnail.extension} /> : ""}

				<div className="Profile-name">
					<h1>{character.name}</h1>
					<h4>{this.state.id}</h4>
				</div>

				{!hasBio ? "" :
					<section>
						<h3>Bio</h3>
						<p>{character.description}</p>
					</section>
				}

				{infoBody}
			</div>
		);
	}
}

export default Profile;