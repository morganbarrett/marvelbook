import React, { Component } from "react";
import md5 from "../md5";
import "isomorphic-fetch";

const HASH_KEY = "8ebd9822fb40f96a13ea894c82c5fafbb65dbe84",
	  PUBLIC_KEY = "8814e69c6105923f6c454429d7e3381c",
	  MARVEL_URL = "https://gateway.marvel.com/v1/public/";

class MarvelAPI extends Component {
	constructor(props){
		super(props);

		this.state = {
			id: undefined,
			query: "",
			data: [],
			loading: false,
			error: false
		};

		this.formatURL = this.formatURL.bind(this);
		this.call = this.call.bind(this);
	}

	formatURL(){
		var time = Date.now(),
			hash = md5.hex_md5(time + HASH_KEY + PUBLIC_KEY),
	    	url = MARVEL_URL + "characters",
	    	params = {
				limit: 100,
				ts: time,
				apikey: PUBLIC_KEY,
				hash: hash
			};

	    if(this.state.id != undefined){
	   		url += "/" + this.state.id;
	    } else if(this.state.query != ""){
	    	params.nameStartsWith = this.state.query;
	    }

	    url += "?";

	    for(var p in params){
			url += p + "=" + params[p] + "&";
	    }

	    return url;
	}

	call(){
		this.setState({
			loading: true,
			error: false
		});

		fetch(this.formatURL(), {
			method: "GET",
			headers: {
				"Accept": "application/json",
				"Content-Type": "application/json",
			}
		}).then(function(response){
    		return response.json();
  		}).then(function(response){
			var state = {loading: false},
				data = response.data;

			if(data && data.total > 0){
				state.data = data.results;

				if(this.state.id != undefined){
					state.data = state.data[0];
				}
			} else {
				state.error = true;
			}

			this.setState(state);
		}.bind(this));
	}
}

export default MarvelAPI;