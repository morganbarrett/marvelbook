import React, { Component } from "react";
import {Link} from "react-router-dom";
import Search from "./Search";
import logo from "../../public/logo.png";
import "../css/Header.css";

class Header extends Component {
	render() {
		return (
			<header>
				<Link to="/">
					<img src={logo} className="Header-logo" alt="logo" />
				</Link>

				<Search></Search>
			</header>
		);
	}
}

export default Header;