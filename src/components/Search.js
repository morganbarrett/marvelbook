import React from "react";
import {Link} from "react-router-dom";
import MarvelAPI from "./MarvelAPI";
import "../css/Search.css";

class Search extends MarvelAPI {
	constructor(props){
		super(props);
		this.update = this.update.bind(this);
		this.search = this.search.bind(this);
		this.call();
	}

	update(event){
		this.setState({
    		query: event.target.value
    	});
	}

	search(event){
    	if(event.key === "Enter"){
    		this.call();
    	}
   	}

	render() {
		var characters = <li className="Search-noResults">No Results</li>;

		if(!this.state.error){
			characters = this.state.data.map(function(character) {
				return (
					<li key={character.id}>
						<Link to={"/profile/" + character.id} >
							<img width="100" height="100" src={character.thumbnail.path + "/standard_medium." + character.thumbnail.extension} />
							<div>{character.name}</div>
						</Link>
					</li>
				);
			});
		}

		return (
			<div>
				<input className="Search" onChange={this.update} onKeyPress={this.search} value={this.state.query} />
				<div className="Search-icon" onClick={this.call}></div>

				<ul className="Search-results">
	    			{characters}
				</ul>
			</div>
		);
	}
}

export default Search;